package org.farriswheel.digitrecognizer

import android.content.res.AssetManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import org.farriswheel.digitrecognizer.ml.MnistModel
import org.jetbrains.bio.npy.NpyFile
import org.jetbrains.kotlinx.multik.api.empty
import org.jetbrains.kotlinx.multik.api.mk
import org.jetbrains.kotlinx.multik.api.ndarray
import org.jetbrains.kotlinx.multik.ndarray.data.D2
import org.jetbrains.kotlinx.multik.ndarray.data.NDArray
import org.jetbrains.kotlinx.multik.ndarray.data.get
import org.jetbrains.kotlinx.multik.ndarray.data.set
import org.jetbrains.kotlinx.multik.ndarray.operations.div
import org.jetbrains.kotlinx.multik.ndarray.operations.plus
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.file.Paths
import kotlin.math.max


class MainActivity : AppCompatActivity() {

    private val model = MnistModel.newInstance(this)


    //private var mDoublePixels = mk.empty<Double, D2>(784, 1)

    /*private lateinit var mLayer1Weights: NDArray<Double, D2>
    private lateinit var mLayer1Biases: NDArray<Double, D2>
    private lateinit var mLayer2Weights: NDArray<Double, D2>
    private lateinit var mLayer2Biases: NDArray<Double, D2>*/

    private lateinit var paintView: PaintView
    private lateinit var resizedImageView: ImageView
    private lateinit var evalLabel: TextView
    private lateinit var probLabel: TextView
    private lateinit var evalButton: Button
    private lateinit var clearButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        paintView = findViewById(R.id.paintView)
        resizedImageView = findViewById(R.id.resizedImageView)
        evalLabel = findViewById(R.id.evaluationLabel)
        probLabel = findViewById(R.id.probabilitiesLabel)
        evalButton = findViewById(R.id.buttonEvaluate)
        clearButton = findViewById(R.id.buttonClear)


        clearButton.setOnClickListener {
            paintView.clear()
            evalLabel.text = ""
        }

        evalButton.setOnClickListener {
            var bitmap = Bitmap.createScaledBitmap(paintView.mBitmap, 28, 28, true)
            val bitmapPixels = IntArray(bitmap.width * bitmap.height)
            bitmap.getPixels(bitmapPixels, 0, 28, 0, 0, 28, 28)

            val doublePixelsArray = FloatArray(784)
            for(i in bitmapPixels.indices) {
                val b = bitmapPixels[i].and(0xFF)
                doublePixelsArray[i] = b.toFloat() / 255.0f
            }
            val b = DoubleBuffer.wrap(doublePixelsArray)

            val inputFeature0 = TensorBuffer.createFixedSize(intArrayOf(1, 28, 28, 1), DataType.FLOAT32)
            inputFeature0.loadBuffer(byteBuffer)


            //mDoublePixels = mk.ndarray(doublePixelsArray, 784, 1)

            /*
            val scaledBitmap = Bitmap.createScaledBitmap(paintView.mBitmap,28,28,true)
            val bitmapBuffer = ByteBuffer.wrap(scaledBitmap.)
            inputFeature0.load(tensorBitmap)
            val outputs = model.process(inputFeature0)
            val outputFeature0 = outputs.outputFeature0AsTensorBuffer

            resizedImageView.setImageBitmap(bitmap)
            evaluate()
            */

            resizedImageView.setImageBitmap(bitmap)
            //evaluate()
        }

        /*val w1_npy = NpyFile.read(Paths.get("/sdcard/w1.npy"))
        Log.w("w1_npy", "Got w1.npy of shape (${w1_npy.shape[0]}, ${w1_npy.shape[1]})")
        mLayer1Weights = mk.ndarray(w1_npy.asDoubleArray(), w1_npy.shape[0], w1_npy.shape[1])

        val w2_npy = NpyFile.read(Paths.get("/sdcard/w2.npy"))
        Log.w("w2_npy", "Got w2.npy of shape (${w2_npy.shape[0]}, ${w2_npy.shape[1]})")
        mLayer2Weights = mk.ndarray(w2_npy.asDoubleArray(), w2_npy.shape[0], w2_npy.shape[1])

        val b1_npy = NpyFile.read(Paths.get("/sdcard/b1.npy"))
        Log.w("b1_npy", "Got b1.npy of shape (${b1_npy.shape[0]}, ${b1_npy.shape[1]})")
        mLayer1Biases = mk.ndarray(b1_npy.asDoubleArray(), b1_npy.shape[0], b1_npy.shape[1])

        val b2_npy = NpyFile.read(Paths.get("/sdcard/b2.npy"))
        Log.w("b2_npy", "Got b2.npy of shape (${b2_npy.shape[0]}, ${b2_npy.shape[1]})")
        mLayer2Biases = mk.ndarray(b2_npy.asDoubleArray(), b2_npy.shape[0], b2_npy.shape[1])*/
    }

    /*private fun ReLU(input: NDArray<Double, D2>) : NDArray<Double, D2> {
        var inputCopy = input.deepCopy()
        for(r in 0 until inputCopy.shape[0]) {
            for(c in 0 until inputCopy.shape[1]) {
                inputCopy[r, c] = max(inputCopy[r, c], 0.0)
            }
        }
        return inputCopy
    }*/

    /*private fun softmax(input: NDArray<Double, D2>) : NDArray<Double, D2> {
        //np.exp(x) / np.sum(np.exp(x), axis=0)
        return mk.math.exp(input) / mk.math.sum(mk.math.exp(input))
    }*/

    /*private fun evaluate() {
        val a1 = mk.linalg.dot(mLayer1Weights, mDoublePixels) + mLayer1Biases
        val o1 = ReLU(a1)

        val a2 = mk.linalg.dot(mLayer2Weights, o1) + mLayer2Biases
        val o2 = softmax(a2)

        var maxIndex = 0
        var max = Double.NEGATIVE_INFINITY
        for(r in 0 until o2.shape[0]) {
            if(o2[r, 0] > max) {
                maxIndex = r
                max = o2[r, 0]
            }
        }
        //Log.w("OUTPUT", a2.toString())
        evalLabel.text = maxIndex.toString()
        probLabel.text = o2.toString()
        //Toast.makeText(this, a2.toString(), Toast.LENGTH_LONG).show()
    }*/
}